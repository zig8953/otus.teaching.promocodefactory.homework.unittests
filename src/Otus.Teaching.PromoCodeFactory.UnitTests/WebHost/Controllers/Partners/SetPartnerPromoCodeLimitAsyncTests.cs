﻿using Xunit;
using AutoFixture;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Providers;
using AutoFixture.AutoMoq;
using System;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using FluentAssertions;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;
        private readonly Mock<ICurrentDateTimeProvider> _currentDateTimeProviderMock;
        private Guid partnerId = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1d99");

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _currentDateTimeProviderMock = fixture.Freeze<Mock<ICurrentDateTimeProvider>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        public Partner CreateBasePartner()
        {
            var partner = new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true,
                NumberIssuedPromoCodes = 5,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2020, 07, 9),
                        EndDate = new DateTime(2020, 10, 9),
                        Limit = 105
                    }
                }
            };

            return partner;
        }

        [Fact]
        public async System.Threading.Tasks.Task SetPartnerPromoCodeLimitAsync_IsNull_NotFond()
        {
            Partner partner = null;

            var request = new SetPartnerPromoCodeLimitRequest();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            Assert.IsType<NotFoundResult> (result);
        }

        [Fact]
        public async System.Threading.Tasks.Task SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_BadRequest()
        {
            var request = new SetPartnerPromoCodeLimitRequest() { EndDate = DateTime.Now, Limit = 10 };
            var  partner = CreateBasePartner();
            partner.IsActive = false;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            result.Should().BeEquivalentTo(_partnersController.BadRequest("Данный партнер не активен"));
        }

        [Fact]
        public async System.Threading.Tasks.Task SetPartnerPromoCodeLimitAsync_ActiveLimit_ResetNumberIssuedPromoCodes()
        {
            var partner = CreateBasePartner();

            var request = new SetPartnerPromoCodeLimitRequest() { Limit = 10 };

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            Assert.Equal(0, partner.NumberIssuedPromoCodes);

        }

        [Fact]
        public async System.Threading.Tasks.Task SetPartnerPromoCodeLimitAsync_ActiveLimit_NotResetNumberIssuedPromoCodes()
        {
            var partner = CreateBasePartner();

            var request = new SetPartnerPromoCodeLimitRequest() { EndDate = DateTime.Now, Limit = 10 };
            int numberIssuedPromoCodes = partner.NumberIssuedPromoCodes;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            Assert.Equal(5, numberIssuedPromoCodes);

        }

        [Fact]
        public async System.Threading.Tasks.Task SetPartnerPromoCodeLimitAsync_ActiveLimit_SetNewLimitCancelOldLimit()
        {
            DateTime nowDate = DateTime.Now;

            var partner = CreateBasePartner();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            _currentDateTimeProviderMock.Setup(x => x.CurrentDateTime).Returns(nowDate);

            var request = new SetPartnerPromoCodeLimitRequest() { Limit = 10 };
 
            var expectedLimit = new PartnerPromoCodeLimit()
            {
                Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                CreateDate = new DateTime(2020, 07, 9),
                EndDate = new DateTime(2020, 10, 9),
                Limit = 105, 
                CancelDate = nowDate
            };

            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            partner.PartnerLimits.First().Should().BeEquivalentTo(expectedLimit);
        }

        [Fact]
        public async System.Threading.Tasks.Task SetPartnerPromoCodeLimitAsync_Limit_AboveZero()
        {
            var partner = CreateBasePartner();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            var request = new SetPartnerPromoCodeLimitRequest() { EndDate = DateTime.Now, Limit = 0 };

            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            Assert.IsNotType<BadRequestResult>(result);
        }

        [Fact]
        public async System.Threading.Tasks.Task SetPartnerPromoCodeLimitAsync_Save_ToBase()
        {
            DateTime endDate = new DateTime(2020, 10, 9);
            DateTime nowdate = new DateTime(2020, 10, 9);

            var partner = CreateBasePartner();
         
            _currentDateTimeProviderMock.Setup(x => x.CurrentDateTime).Returns(nowdate);

            var request = new SetPartnerPromoCodeLimitRequest() { EndDate = endDate, Limit = 10 };

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);          

            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            PartnerPromoCodeLimit expectedNewLimit = new PartnerPromoCodeLimit()
            {
                CancelDate = null,
                CreateDate = endDate,
                EndDate = endDate,
                Limit = 10,
                Partner = partner,
                PartnerId = partner.Id
            };

            var partnerPromoCodeLimit = new List<PartnerPromoCodeLimit> { partner.PartnerLimits.First(), expectedNewLimit };

            partner.PartnerLimits.Should().BeEquivalentTo(partnerPromoCodeLimit);
        }
    }
}