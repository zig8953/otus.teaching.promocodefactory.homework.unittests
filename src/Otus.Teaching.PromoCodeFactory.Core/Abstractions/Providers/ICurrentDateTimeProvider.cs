﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Providers
{
    public interface ICurrentDateTimeProvider
    {
        DateTime CurrentDateTime { get; }
    }
}
